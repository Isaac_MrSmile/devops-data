ELASTICSEARCH CLUSTER

En los dos nuevos nodos que van a pasar a formar parte del clúster deberá ejecutarse los mismos pasos
que se siguieron en el módulo 3 para la preparación de las dos máquinas virtuales nuevas y la posterior
instalación y configuración de Elasticsearch.

CLUSTER CREATION


Para conseguir que cada uno de los nodos deje de trabajar por separado y todos pasen a formar parte del
mismo cúster se deberá modificar el archivo de configuración del servico "elasticsearch.yml"
añadiendo los dos siguientes parámetros:


discovery.zen.ping.unicast.hosts: ["192.168.1.31", "192.168.1.32", "192.168.1.33"]
discovery.zen.minimum_master_nodes: 2



Se puede modificar también la configuración de Kibana para que apunte a los tres nodos en vez de solo a
uno.
9