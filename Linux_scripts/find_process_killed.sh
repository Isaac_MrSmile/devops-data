#!/bin/bash
your_job_here
ret=$?
#
#  returns > 127 are a SIGNAL
#
if [ $ret -gt 127 ]; then
        sig=$((ret - 128))
        echo "Got SIGNAL $sig"
        if [ $sig -eq $(kill -l SIGKILL) ]; then
                echo "process was killed with SIGKILL"
                dmesg > $HOME/dmesg-kill.log
        fi
fi

#Note: "your_job_here" is the name of the program/job you want to run. 
#This script checks the return code of the program and will check if it was killed with a SIGKILL and if so, 
#will dump the dmesg immediately afterwards to your home directory in a file called dmesg-kill.log